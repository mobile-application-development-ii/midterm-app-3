package com.defalt.app3.model

import androidx.annotation.StringRes


data class Gasoline(
    @StringRes val stringResourcePrice: Int,
    @StringRes val stringResourceOil: Int,
)
