package com.defalt.app3.data

import com.defalt.app3.R
import com.defalt.app3.model.Gasoline

class Datasource {
    fun loadGasoline(): List<Gasoline> {
        return listOf<Gasoline>(
            Gasoline(R.string.price1,R.string.oil1),
            Gasoline(R.string.price2,R.string.oil2),
            Gasoline(R.string.price3,R.string.oil3),
            Gasoline(R.string.price4,R.string.oil4),
            Gasoline(R.string.price5,R.string.oil5),
            Gasoline(R.string.price6,R.string.oil6),
            Gasoline(R.string.price7,R.string.oil7),
            Gasoline(R.string.price8,R.string.oil8),
            Gasoline(R.string.price9,R.string.oil9),

            )
    }
}